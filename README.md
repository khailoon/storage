Storage
=======

This library is an Atlassian plugin that provides a common API for accessing 
stored data in number of formats, with different back-ends.

