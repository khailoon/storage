/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.storage;

import java.util.Collections;
import java.util.Set;

/**
 * This storage implementation will act like it is storing, but will never actually
 * store any data. This is useful in situations where a storage instance is expected but no
 * actual storing is done and it's simpler than adding <code>!= null</code> tests everywhere.
 *
 * @author David Peterson
 */
public class EmptyStorage extends ObjectBasedStorage
{
    private boolean isReadOnly;

    /**
     * Constructs a new empty storage location which will never
     * store any data entered into it.
     */
    public EmptyStorage()
    {
        this(false);
    }

    /**
     * @param isReadOnly Sets whether this storage will report itself as
     *                   read-only.
     */
    public EmptyStorage(boolean isReadOnly)
    {
        super(BoxType.Virtual);
        this.isReadOnly = isReadOnly;
    }

    /**
     * @return an empty set.
     */
    @Override protected Set<String> baseNameSet()
    {
        return Collections.EMPTY_SET;
    }

    /**
     * Returns an object stored with the specified name.
     *
     * @param name The name of the value to return.
     * @return the stored object.
     */
    @Override protected Object getBaseObject(String name)
    {
        return null;
    }

    /**
     * Sets the object in the base object.
     *
     * @param name  The name of the value.
     * @param value The object value to set.
     */
    @Override protected void setBaseObject(String name, Object value)
    {
        // Do nothing...
    }

    /**
     * @see Storage#isReadOnly()
     */
    @Override public boolean isReadOnly()
    {
        return isReadOnly;
    }
}
