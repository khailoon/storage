/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.storage;

import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;

/**
 * This is an abstract base-class for storage locations which allow the storage
 * of objects.
 * 
 * @author David Peterson
 */
public abstract class ObjectBasedStorage extends BasedStorage {

    private static final Logger LOG = Logger.getLogger( ObjectBasedStorage.class );

    /**
     * @param boxType
     */
    public ObjectBasedStorage( BoxType boxType ) {
        super( boxType );
    }

    /**
     * Returns the base object for the named value, checking if it is an
     * instance of the supplied class. If it is not, <code>null</code> is
     * returned.
     * 
     * @param name
     *            The object name.
     * @param clazz
     *            The class the object must be an instance of. May not be
     *            <code>null</code>.
     * @return The object, or <code>null</code> if the object is not set, or
     *         is not an instance of the class.
     */
    protected <T> T getBaseObject( String name, Class<T> clazz ) {
        Object value = getBaseObject( name );
        return toType( value, null, clazz );
    }

    @Override
    protected Boolean getBaseBoolean( String name ) {
        return getBaseObject( name, Boolean.class );
    }

    @Override
    protected Date getBaseDate( String name ) {
        return getBaseObject( name, Date.class );
    }

    @Override
    protected Double getBaseDouble( String name ) {
        if ( LOG.isDebugEnabled() )
            LOG.debug( "Value for " + name + ": " + getBaseNumber( name ) );

        final Number theNumber = getBaseNumber( name );
        return null == theNumber ? null : new Double( theNumber.doubleValue() );
    }

    @Override
    protected Integer getBaseInteger( String name ) {
        if ( LOG.isDebugEnabled() )
            LOG.debug( "Value for " + name + ": " + getBaseNumber( name ) );

        final Number theNumber = getBaseNumber( name );
        return null == theNumber ? null : new Integer( theNumber.intValue() );
    }

    @Override
    protected Long getBaseLong( String name ) {
        if ( LOG.isDebugEnabled() )
            LOG.debug( "Value for " + name + ": " + getBaseNumber( name ) );

        final Number theNumber = getBaseNumber( name );
        return null == theNumber ? null : new Long( theNumber.longValue() );
    }

    /**
     * Returns the named value as a Number. Useful if the specific type of
     * number in the field is unknown.
     * 
     * @param name
     *            The name of the value.
     * @return The value as a Number.
     */
    @Override
    protected Number getBaseNumber( String name ) {
        return getBaseObject( name, Number.class );
    }

    @Override
    protected List<?> getBaseObjectList( String name ) {
        return getBaseObject( name, List.class );
    }

    @Override
    protected String getBaseString( String name ) {
        return ( String ) getBaseObject( name, String.class );
    }

    @Override
    protected String[] getBaseStringArray( String name ) {
        return ( String[] ) getBaseObject( name );
    }

    @Override
    protected void setBaseBoolean( String name, Boolean value ) {
        setBaseObject( name, value );
    }

    @Override
    protected void setBaseDate( String name, Date value ) {
        setBaseObject( name, value );
    }

    @Override
    protected void setBaseDouble( String name, Double value ) {
        setBaseObject( name, value );
    }

    @Override
    protected void setBaseInteger( String name, Integer value ) {
        setBaseObject( name, value );
    }

    @Override
    protected void setBaseLong( String name, Long value ) {
        setBaseObject( name, value );
    }

    @Override
    protected void setBaseObjectList( String name, List<?> value ) {
        setBaseObject( name, value );
    }

    @Override
    protected void setBaseString( String name, String value ) {
        setBaseObject( name, value );
    }

    @Override
    protected void setBaseStringArray( String name, String[] value ) {
        setBaseObject( name, value );
    }

}
