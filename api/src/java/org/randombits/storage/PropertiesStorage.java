/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.storage;

import java.util.Properties;
import java.util.Set;

/**
 * This implementation of {@link Storage} has a Properties object at it's base.
 */
public class PropertiesStorage extends StringBasedStorage {

    private Properties properties;

    /**
     * Constructs a new instance with the specified properties as the base.
     *
     * @param props The properties.
     */
    public PropertiesStorage(Properties props) {
        super(BoxType.Virtual);
        this.properties = props;
    }

    /**
     * Returns the base set of names.
     */
    @Override protected Set<String> baseNameSet() {
        if (properties != null) {
            Set<String> cProperties = new java.util.HashSet<String>();
            for ( Object key : properties.keySet() )
                cProperties.add( (String)key );
            return cProperties;
        }
        return null;
    }

    /**
     * Returns a string from the base object.
     */
    @Override protected String getBaseString(String name) {
        if (properties != null)
            return properties.getProperty(name);
        return null;
    }

    /**
     * Sets a string in the base object.
     */
    @Override protected void setBaseString(String name, String value) {
        checkReadOnly();
        
        if ( properties != null )
            properties.setProperty( name, value );
    }

    /**
     * @return <code>false</code>.
     */
    @Override public boolean isReadOnly() {
        return false;
    }
}
