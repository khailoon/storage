package org.randombits.storage;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Enumeration;
import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Provides support for simplifying ResourceBundle access. In particular, it can
 * automatically load resource bundles for path names and classes without
 * throwing exceptions if they are missing.
 * 
 * <p>
 * Additionally, it adds support for injecting {@link MessageFormat} arguments
 * into the returned String values automatically, via the
 * {@link #getMessage(String, String, Object...)} method.
 * 
 * @author David Peterson
 */
public class ResourceStorage extends StringBasedStorage {

    private ResourceBundle resourceBundle;

    private Set<String> keySet;

    private ResourceStorage() {
        super( BoxType.Virtual );
    }

    /**
     * Constructs a new instance with the specified resource bundle as its base.
     * 
     * @param resourceBundle
     *            The resource bundle.
     */
    public ResourceStorage( ResourceBundle resourceBundle ) {
        this();
        this.resourceBundle = resourceBundle;
    }

    public ResourceStorage( String name ) {
        this( name, Locale.getDefault() );
    }

    public ResourceStorage( String name, Locale locale ) {
        this( name, locale, ResourceStorage.class.getClassLoader() );
    }

    /**
     * Constructs a new instance, looking for the named properties file. Eg,
     * 'foo.bar' will look for 'foo/bar.properties' and other local-specific
     * variants.
     * 
     * @param name
     *            The name of the resource.
     * @param locale
     *            The locale.
     * @param classLoader
     *            The class loader to look in.
     */
    public ResourceStorage( String name, Locale locale, ClassLoader classLoader ) {
        this();
        resourceBundle = findResourceBundle( name, locale, classLoader );
    }

    /**
     * This will find the resource bundle for the specified class type, if one
     * is available. The default locale and the <code>type</code>'s classloader
     * will be used.
     * 
     * @param type
     *            The class to use.
     */
    public ResourceStorage( Class<?> type ) {
        this( type, Locale.getDefault() );
    }

    /**
     * This will find the resource bundle for the specified class type, if one
     * is available. The <code>type</code>'s classloader will be used.
     * 
     * @param type
     *            The class to find the bundle for.
     * @param locale
     *            The locale to load.
     */
    public ResourceStorage( Class<?> type, Locale locale ) {
        this( type.getName(), locale, type.getClassLoader() );
    }

    /**
     * This method is called when initialising the resource bundle.
     * 
     * @return the resource bundle for this command.
     */
    protected ResourceBundle findResourceBundle( String name, Locale locale, ClassLoader classLoader ) {
        try {
            return ResourceBundle.getBundle( name, locale, classLoader );
        } catch ( MissingResourceException e ) {
            return new ListResourceBundle() {
                @Override
                protected Object[][] getContents() {
                    return new Object[][]{};
                }
            };
        }
    }

    public ResourceBundle getBundle() {
        return resourceBundle;
    }

    @Override
    protected Set<String> baseNameSet() {
        if ( resourceBundle != null && keySet == null ) {
            keySet = new java.util.HashSet<String>();
            Enumeration<String> keys = resourceBundle.getKeys();
            while ( keys.hasMoreElements() ) {
                keySet.add( keys.nextElement() );
            }
        }
        return keySet;
    }

    @Override
    protected String getBaseString( String name ) {
        try {
            if ( resourceBundle != null ) {
                return resourceBundle.getString( name );
            }
        } catch ( MissingResourceException e ) {
            // Do nothing.
        }
        return null;
    }

    /**
     * Retrieves a string value with the specified name. The arguments will be
     * injected into the returned value via the {@link MessageFormat} class. If
     * no value is found for the name, the default will be used. If the default
     * is also <code>null</code>, no injection is performed and
     * <code>null</code> is returned.
     * 
     * @param name
     *            The name of the key to find.
     * @param def
     *            The default value to use if no value is found.
     * @param arguments
     *            The list of arguments to inject into the returned value.
     * @return The injected value, or <code>null</code> if none was found.
     */
    public String getMessage( String name, String def, Object... arguments ) {
        String value = getString( name, def );
        if ( value != null && arguments != null && arguments.length > 0 ) {
            value = MessageFormat.format( value, arguments );
        }
        return value;
    }

    public String getMessage( String name, String def, Collection<?> arguments ) {
        Object[] args = null;

        if ( arguments != null ) {
            args = new Object[arguments.size()];
            int i = 0;
            for ( Object arg : arguments )
                args[i++] = arg;
        }
        return getMessage( name, def, args );
    }

    /**
     * Returns the string value for the specified name. If no value is found,
     * the <code>name</code> will be returned as the value.
     * 
     * @param name
     *            The name.
     * @return The string value, or <code>name</code>.
     */
    public String getMessage( String name ) {
        return getMessage( name, name );
    }

    @Override
    protected void setBaseString( String name, String value ) {
        checkReadOnly();
    }

    @Override
    public final boolean isReadOnly() {
        return true;
    }

}
