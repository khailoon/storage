/*
 * Copyright (c) 2005, David Peterson 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * 			 notice, this list of conditions and the following disclaimer in the
 *   		 documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 * 			 may be used to endorse or promote products derived from this software
 * 			 without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * v1.0: Created on Oct 10, 2005 by David Peterson
 */
package org.randombits.storage.confluence;

import java.util.Set;

import org.randombits.storage.ObjectBasedStorage;

import com.atlassian.bandana.BandanaContext;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.spring.container.ContainerManager;

/**
 * 
 * @author David Peterson
 */
public class BandanaStorage extends ObjectBasedStorage
{
    private BandanaContext bandanaContext;

    private BandanaManager bandanaManager;

    /**
     * Constructs a new BandanaStorage instance, looking up the system bandana
     * manager.
     * 
     * @param type
     *            Either {@link BoxType#Real} or {@link BoxType#Virtual}.
     * @param ctx
     *            The bandana context the storage instance accesses.
     */
    public BandanaStorage(BoxType type, BandanaContext ctx)
    {
        this(type, ctx, (BandanaManager) ContainerManager.getComponent("bandanaManager"));
    }

    /**
     * Construcats a new BandanaStorage instance, using the specified bandana
     * manager.
     * 
     * @param type
     *            The box type.
     * @param ctx
     *            The bandana context the storage instance accesses.
     * @param manager
     *            The bandana manager.
     */
    public BandanaStorage(BoxType type, BandanaContext ctx, BandanaManager manager)
    {
        super(type);
        bandanaContext = ctx;
        bandanaManager = manager;
    }

    @Override protected Set<String> baseNameSet()
    {
        return null;
    }

    @Override protected Object getBaseObject(String name)
    {
        return bandanaManager.getValue(bandanaContext, name);
    }

    @Override public boolean isReadOnly()
    {
        return false;
    }

    @Override protected void setBaseObject(String name, Object value)
    {
        // BUG: There is a bug in the bandana manager which prevents storing
        // null objects.
//        if (value != null)
            bandanaManager.setValue(bandanaContext, name, value);
    }

}
