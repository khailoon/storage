/*
 * Copyright (c) 2005, David Peterson 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * 			 notice, this list of conditions and the following disclaimer in the
 *   		 documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 * 			 may be used to endorse or promote products derived from this software
 * 			 without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * v1.0: Created on Sep 20, 2005 by David Peterson
 */
package org.randombits.storage.confluence;

import java.util.Set;

import org.randombits.storage.StringBasedStorage;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.spring.container.ContainerManager;

/**
 * Stores data as content properties. This version uses maps instead of property
 * names for boxes, allowing more flexibility in tracking and deleting data.
 * 
 * @author David Peterson
 */
public class ContentPropertyStorage extends StringBasedStorage
{

    /**
     * 
     */
    private static final String    CONTENT_PROPERTY_MANAGER = "contentPropertyManager";

    private ContentEntityObject    content;

    private ContentPropertyManager contentPropertyManager;

    /**
     * Constructs a new ContentPropertyStorage object which uses maps for boxes.
     * 
     * @param content
     *            The content to access.
     */
    public ContentPropertyStorage(ContentEntityObject content)
    {
        this(content, true);
    }

    /**
     * Constructs a new ContentPropertyStorage object.
     * 
     * @param content
     *            The content the properties are stored against.
     * @param useMaps
     *            If <code>true</code>, Maps will be used for storing boxes
     *            rather than named parameters.
     */
    public ContentPropertyStorage(ContentEntityObject content, boolean useMaps)
    {
        this(content, useMaps, (ContentPropertyManager) ContainerManager
                .getComponent(CONTENT_PROPERTY_MANAGER));
    }

    /**
     * Constructs a new ContentPropertyStorage object.
     * 
     * @param content
     *            The content object the properties are stored against.
     * @param useMaps
     *            If <code>true</code>, Maps will be used for storing boxes
     *            rather than named properties.
     * @param manager
     *            The content property manager to use. This saves a component
     *            lookup operation if you already have it.
     */
    public ContentPropertyStorage(ContentEntityObject content, boolean useMaps,
            ContentPropertyManager manager)
    {
        super(useMaps ? BoxType.Real : BoxType.Virtual );
        
        if (content == null)
            throw new IllegalArgumentException("The content may not be null.");
        
        this.content = content;
        contentPropertyManager = manager;
    }

    @Override protected Set<String> baseNameSet()
    {
        return null;
    }

    @Override protected String getBaseString(String name)
    {
        return contentPropertyManager.getTextProperty(content, name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.Storage#isReadOnly()
     */
    @Override public boolean isReadOnly()
    {
        return false;
    }

    @Override protected void setBaseString(String name, String value)
    {
        contentPropertyManager.setTextProperty(content, name, value);
    }

    /**
     * @param contentPropertyManager
     *            The content property manager.
     */
    public void setContentPropertyManager(
            ContentPropertyManager contentPropertyManager)
    {
        this.contentPropertyManager = contentPropertyManager;
    }

    /**
     * @return The content object this storage operates on.
     */
    public ContentEntityObject getContent()
    {
        return content;
    }
}
