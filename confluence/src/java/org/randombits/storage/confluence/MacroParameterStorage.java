/*
 * Copyright (c) 2005, David Peterson 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * 			 notice, this list of conditions and the following disclaimer in the
 *   		 documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 * 			 may be used to endorse or promote products derived from this software
 * 			 without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * v1.0: Created on Sep 21, 2005 by David Peterson
 */
package org.randombits.storage.confluence;

import org.joda.time.DateTime;
import org.randombits.storage.IndexedStorage;
import org.randombits.storage.StringBasedStorage;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Provides a storage implementation for accessing macro parameters. Basically,
 * this adds the ability to access parameters by index (1..x) as well as by
 * their name.
 * 
 * <p>
 * Also, this class will make parameter names case-insensitive, so calling
 * <code>getString("FooBar", null)</code> will return the same result as
 * calling <code>getString("foobar", null)</code>.
 * 
 * <p>
 * However, the {@link #nameSet()} method returns the set of names in their
 * original case.
 * 
 * @author David Peterson
 */
public class MacroParameterStorage extends StringBasedStorage implements IndexedStorage {
    private Map<String, String> originalParams;

    private Map<String, String> params;

    /**
     * Creates a storage instance with the specified Map as the base.
     * 
     * @param params
     *            The parameter map.
     */
    public MacroParameterStorage( Map<String, String> params ) {
        super( BoxType.Virtual );

        cleanIndexedParams( params );

        this.originalParams = params;
        this.params = toLowerCase( params );
    }

    private Map<String, String> toLowerCase( Map<String, String> params ) {
        Map<String, String> lcParams = new java.util.HashMap<String, String>();
        for ( Map.Entry<String, String> e : params.entrySet() ) {
            lcParams.put( e.getKey().toLowerCase(), e.getValue() );
        }
        return lcParams;
    }

    private void cleanIndexedParams( Map<String, String> params ) {
        String value, index;
        int i = 0;

        do {
            index = String.valueOf( i++ );
            value = params.get( index );
            if ( value != null && value.indexOf( '=' ) >= 0 )
                params.remove( index );
        } while ( value != null );
    }

    @Override public boolean isReadOnly() {
        return true;
    }

    /**
     * Retrieves the value at the unnamed indexed parameter.
     * 
     * @param index
     *            The index of the unnamed parameter.
     * @param def
     *            The value to return if the parameter is not set.
     * @return the value of the indexed parameter, or the default value.
     */
    public String getString( int index, String def ) {
        return getString( String.valueOf( index ), def );
    }

    @Override protected Set<String> baseNameSet() {
        return originalParams.keySet();
    }

    @Override protected String[] getBaseStringArray( String name ) {
        String value = getBaseString( name );
        if ( value != null ) {
            return value.split( "[,;]" );
        }
        return null;
    }

    @Override protected String getBaseString( String name ) {
        if ( name != null )
            return ( String ) params.get( name.toLowerCase() );
        return null;
    }

    @Override protected void setBaseString( String name, String value ) {
        checkReadOnly();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#findBoolean(int, boolean)
     */
    public boolean getBoolean( int index, boolean def ) {
        return getBoolean( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#findBoolean(int,
     *      java.lang.Boolean)
     */
    public Boolean getBoolean( int index, Boolean def ) {
        return getBoolean( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#getDate(int, java.util.Date)
     */
    public Date getDate( int index, Date def ) {
        return getDate( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#getDateTime(int,
     *      org.joda.time.DateTime)
     */
    public DateTime getDateTime( int index, DateTime def ) {
        return getDateTime( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#getDouble(int, double)
     */
    public double getDouble( int index, double def ) {
        return getDouble( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#getDouble(int,
     *      java.lang.Double)
     */
    public Double getDouble( int index, Double def ) {
        return getDouble( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#getInteger(int, int)
     */
    public int getInteger( int index, int def ) {
        return getInteger( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#getInteger(int,
     *      java.lang.Integer)
     */
    public Integer getInteger( int index, Integer def ) {
        return getInteger( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#getLong(int, long)
     */
    public long getLong( int index, long def ) {
        return getLong( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#getLong(int, java.lang.Long)
     */
    public Long getLong( int index, Long def ) {
        return getLong( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#getObject(int,
     *      java.lang.Object)
     */
    public Object getObject( int index, Object def ) {
        return getObject( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#getObjectList(int,
     *      java.util.List)
     */
    public List<?> getObjectList( int index, List<?> def ) {
        return getObjectList( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#getStringArray(int,
     *      java.lang.String[])
     */
    public String[] getStringArray( int index, String[] def ) {
        return getStringArray( String.valueOf( index ), def );
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.randombits.storage.IndexedStorage#getStringSet(int,
     *      java.util.Set)
     */
    public Set<String> getStringSet( int index, Set<String> def ) {
        return getStringSet( String.valueOf( index ), def );
    }

    public Map<String, String> getOriginalParams() {
        return originalParams;
    }

    public Map<String, String> getParams() {
        return params;
    }
}
