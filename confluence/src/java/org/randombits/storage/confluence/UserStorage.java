/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.storage.confluence;

import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;
import com.opensymphony.module.propertyset.PropertySet;
import org.randombits.storage.BasedStorage;
import org.randombits.storage.StorageException;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Allows storage against a {@link User}.
 * 
 * @since 1.1
 * @author David Peterson
 */
public class UserStorage extends BasedStorage {
    private User user;

    private PropertySet propertySet;

    private UserAccessor userAccessor;

    public UserStorage( User user ) {
        super( BoxType.Virtual );
        this.user = user;
        propertySet = getUserAccessor().getPropertySet( user );
    }

    private UserAccessor getUserAccessor() {
        if ( userAccessor == null )
            userAccessor = ( UserAccessor ) ContainerManager.getComponent( "userAccessor" );
        return userAccessor;
    }

    /**
     * Returns the name set for the base object.
     * 
     * @return The name set, or <code>null</code> if the set is not available.
     */
    @Override
    protected Set<String> baseNameSet() {
        return new HashSet<String>( propertySet.getKeys() );
    }

    /**
     * Retrieves a Boolean from the base object.
     * 
     * @param string
     *            The name of the boolean value.
     * @return the Boolean value for the specified name.
     */
    @Override
    protected Boolean getBaseBoolean( String string ) {
        return Boolean.valueOf( propertySet.getBoolean( string ) );
    }

    /**
     * Retrieves the named DateTime value from the base object.
     * 
     * @param name
     *            The name of the value.
     * @return The value.
     * @throws org.randombits.storage.StorageException
     *             if there is a problem retrieving the date.
     */
    @Override
    protected Date getBaseDate( String name ) {
        return propertySet.getDate( name );
    }

    /**
     * Retrieves the double value from the base object.
     * 
     * @param name
     *            The name of the value.
     * @return The value.
     */
    @Override
    protected Double getBaseDouble( String name ) {
        return new Double( propertySet.getDouble( name ) );
    }

    /**
     * Returns an integer from the base object.
     * 
     * @param name
     *            The named value.
     * @return The integer object, or <code>null</code>.
     */
    @Override
    protected Integer getBaseInteger( String name ) {
        return new Integer( propertySet.getInt( name ) );
    }

    /**
     * Returns the named value as a Long.
     * 
     * @param name
     *            The name of the value.
     * @return The value as a long.
     */
    @Override
    protected Long getBaseLong( String name ) {
        return new Long( propertySet.getLong( name ) );
    }

    /**
     * Returns the named value as a Number. Useful if the specific type of
     * number in the field is unknown.
     * 
     * @param name
     *            The name of the value.
     * @return The value as a Number.
     */
    @Override
    protected Number getBaseNumber( String name ) {
        return toType( propertySet.getAsActualType( name ), null, Number.class );
    }

    /**
     * Returns an object stored with the specified name.
     * 
     * @param name
     *            The name of the value to return.
     * @return the stored object.
     */
    @Override
    protected Object getBaseObject( String name ) {
        return propertySet.getObject( name );
    }

    /**
     * Retrieves the object list from the underlying base object.
     * 
     * @param name
     *            The name of the stored list.
     * @return The list.
     */
    @Override
    protected List<?> getBaseObjectList( String name ) {
        return toType( propertySet.getObject( name ), null, List.class );
    }

    /**
     * Returns the named string value from the base object.
     * 
     * @param name
     *            The name of the string to return.
     * @return the string value, or <code>null</code>.
     */
    @Override
    protected String getBaseString( String name ) {
        return propertySet.getString( name );
    }

    /**
     * Returns the string array from the underlying base object.
     * 
     * @param name
     *            The name of the value.
     * @return The string array.
     * @throws org.randombits.storage.StorageException
     *             if there is a problem retrieving the value.
     */
    @Override
    protected String[] getBaseStringArray( String name ) throws StorageException {
        return toType( propertySet.getObject( name ), null, String[].class );
    }

    /**
     * Sets the named boolean value in the base object.
     * 
     * @param name
     *            The name of the value.
     * @param value
     *            The value being set.
     */
    @Override
    protected void setBaseBoolean( String name, Boolean value ) {
        if ( value != null )
            propertySet.setBoolean( name, value.booleanValue() );
        else
            propertySet.remove( name );
    }

    /**
     * Stores the named date value in the base object.
     * 
     * @param name
     *            The name of the value.
     * @param value
     *            The Date value being set.
     */
    @Override
    protected void setBaseDate( String name, Date value ) {
        if ( value != null )
            propertySet.setDate( name, value );
        else
            propertySet.remove( name );
    }

    /**
     * Sets the named double value in the base object.
     * 
     * @param name
     *            The name of the value.
     * @param value
     *            The value being set.
     */
    @Override
    protected void setBaseDouble( String name, Double value ) {
        if ( value != null )
            propertySet.setDouble( name, value.doubleValue() );
        else
            propertySet.remove( name );
    }

    /**
     * Sets the integer in the base object.
     * 
     * @param name
     *            The name of the value.
     * @param value
     *            The integer value to set.
     */
    @Override
    protected void setBaseInteger( String name, Integer value ) {
        if ( value != null )
            propertySet.setInt( name, value.intValue() );
        else
            propertySet.remove( name );
    }

    /**
     * Sets the long in the base object.
     * 
     * @param name
     *            The name of the value.
     * @param value
     *            The long value to set.
     */
    @Override
    protected void setBaseLong( String name, Long value ) {
        if ( value != null )
            propertySet.setLong( name, value.longValue() );
        else
            propertySet.remove( name );
    }

    /**
     * Sets the object in the base object.
     * 
     * @param name
     *            The name of the value.
     * @param value
     *            The object value to set.
     */
    @Override
    protected void setBaseObject( String name, Object value ) {
        if ( value != null )
            propertySet.setObject( name, value );
        else
            propertySet.remove( name );
    }

    /**
     * Stores the list into the base object.
     * 
     * @param name
     *            The name to store with.
     * @param value
     *            The value to store.
     */
    @Override
    protected void setBaseObjectList( String name, List<?> value ) {
        setBaseObject( name, value );
    }

    /**
     * Set the string value in the base object.
     * 
     * @param name
     *            The name of the value.
     * @param value
     *            The value to set.
     */
    @Override
    protected void setBaseString( String name, String value ) {
        if ( value != null )
            propertySet.setString( name, value );
        else
            propertySet.remove( name );
    }

    /**
     * Sets the string array in the underlying base object.
     * 
     * @param name
     *            The name to store the value as.
     * @param value
     *            The value to store.
     */
    @Override
    protected void setBaseStringArray( String name, String[] value ) {
        setBaseObject( name, value );
    }

    /**
     * @see org.randombits.storage.Storage#isReadOnly()
     */
    @Override
    public boolean isReadOnly() {
        return false;
    }

    public User getUser() {
        return user;
    }
}
