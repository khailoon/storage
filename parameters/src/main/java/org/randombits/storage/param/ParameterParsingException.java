package org.randombits.storage.param;

public class ParameterParsingException extends Exception {

    public ParameterParsingException() {
        // TODO Auto-generated constructor stub
    }

    public ParameterParsingException( String message ) {
        super( message );
        // TODO Auto-generated constructor stub
    }

    public ParameterParsingException( Throwable cause ) {
        super( cause );
        // TODO Auto-generated constructor stub
    }

    public ParameterParsingException( String message, Throwable cause ) {
        super( message, cause );
        // TODO Auto-generated constructor stub
    }

}
