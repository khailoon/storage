package org.randombits.storage.param;

import java.util.regex.Pattern;

import junit.framework.TestCase;

public class ParameterParserTest extends TestCase {

    @Override protected void setUp() throws Exception {
        super.setUp();
    }

    @Override protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testParameterParserChars() {
        ParameterParser parser = new ParameterParser( '=', '|', '\\' );
        assertEquals( '=', parser.getAssignmentSeparator() );
        assertEquals( '|', parser.getParamSeparator() );
        assertEquals( '\\', parser.getEscape() );
    }

    public void testCompileEscapedSplitterChars() {
        Pattern splitter = ParameterParser.compileEscapedSplitter( '\\', '=' );
        testSplitterPatterns( splitter );
    }

    private void testSplitterPatterns( Pattern splitter ) {
        testSplitterPattern( splitter, "foo=bar", new String[]{"foo", "bar"} );
        testSplitterPattern( splitter, "f\\=oo=bar\\=ed", new String[]{"f\\=oo", "bar\\=ed"} );
        testSplitterPattern( splitter, "f=oo=bar=ed", new String[]{"f", "oo", "bar", "ed"} );
        testSplitterPattern( splitter, "f=oo\\=bar=ed", new String[]{"f", "oo\\=bar", "ed"} );
        testSplitterPattern( splitter, "f=o\\o=bar=ed", new String[]{"f", "o\\o", "bar", "ed"} );
        testSplitterPattern( splitter, "f=o\\\\o=bar=ed", new String[]{"f", "o\\\\o", "bar", "ed"} );
        testSplitterPattern( splitter, "f=oo\\\\=bar=ed", new String[]{"f", "oo\\\\", "bar", "ed"} );
        testSplitterPattern( splitter, "=bar", new String[]{"", "bar"} );
    }

    private void testSplitterPattern( Pattern splitter, String original, String[] split ) {
        assertEquals( split, splitter.split( original ) );
    }

    private void assertEquals( Object[] a1, Object[] a2 ) {
        if ( a1 == null ) {
            assertNull( a2 );
            return;
        }

        assertEquals( a1.length, a2.length );
        for ( int i = 0; i < a1.length; i++ ) {
            assertEquals( a1[i], a2[i] );
        }
    }

    public void testParseList() {
        ParameterParser parser = new ParameterParser( '=', '|', '\\' );

        /* ===== Named parameters only ===== */

        // These cases should pass.
        testParseList( parser, true, "foo=bar", new String[][]{{"foo", "bar"}} );
        testParseList( parser, true, "\\==bar", new String[][]{{"=", "bar"}} );
        testParseList( parser, true, "foo=bar|b\\ob=m\\arl\\ey", new String[][]{{"foo", "bar"}, {"bob", "marley"}} );

        // Multiple params with the same name will end up as the last value.
        testParseList( parser, true, "foo=bar|foo=rab", new String[][]{{"foo", "rab"}} );

        // These cases should fail.
        testParseList( parser, true, "=bar", null );
        testParseList( parser, true, "bar", null );

        /* ===== Unnamed is allowed ===== */

        testParseList( parser, false, "foo=bar", new String[][]{{"foo", "bar"}} );
        testParseList( parser, false, "\\==bar", new String[][]{{"=", "bar"}} );
        testParseList( parser, false, "foo=bar|b\\ob=m\\arl\\ey",
                new String[][]{{"foo", "bar"}, {"bob", "marley"}} );

        // These cases should pass.
        testParseList( parser, false, "=bar", new String[][]{{"0", "bar"}} );
        testParseList( parser, false, "bar", new String[][]{{"0", "bar"}} );

        testParseList( parser, false, "bar", new String[][]{{"0", "bar"}} );

        testParseList( parser, false, "foo|bar|foo=bar",
                new String[][]{{"0", "foo"}, {"1", "bar"}, {"foo", "bar"}} );

        testParseList( parser, false, "foo=bar\\|bob=marley", new String[][]{{"foo", "bar|bob=marley"}} );

        // Unnamed params are only allowed at the start of the list.
        testParseList( parser, false, "foo=bar|foo|bar", null );
    }

    private void testParseList( ParameterParser parser, boolean requireKey, String paramList, String[][] paramCheck ) {
        try {
            ParameterStorage<String> storage = parser.parseList( paramList, requireKey );
            if ( paramCheck == null )
                fail( "Expected an exception to be thrown." );

            assertEquals( paramCheck.length, storage.nameSet().size() );
            for ( String[] param : paramCheck ) {
                assertEquals( param[1], storage.getString( param[0], null ) );
            }
        } catch ( ParameterParsingException e ) {
            if ( paramCheck != null )
                fail( e.getMessage() );
        }
    }

    public void testParseParameter() {
        ParameterParser parser = new ParameterParser( ':', ';', '@' );

        testParseParameter( parser, true, "foo:bar", new Parameter<String>( "foo", "bar" ) );
        testParseParameter( parser, true, "foo@:bar:widget", new Parameter<String>( "foo:bar", "widget" ) );
        testParseParameter( parser, true, "foo", null );

        testParseParameter( parser, false, "foo:bar", new Parameter<String>( "foo", "bar" ) );
        testParseParameter( parser, false, "foo@:bar:widget", new Parameter<String>( "foo:bar", "widget" ) );
        testParseParameter( parser, false, "foo", new Parameter<String>( null, "foo" ) );

    }

    private void testParseParameter( ParameterParser parser, boolean requireKey, String paramValue,
            Parameter<String> match ) {
        try {
            Parameter<String> param = parser.parseParameter( paramValue, requireKey );
            if ( match == null )
                fail( "Expected an exception to be thrown" );
            assertEquals( match, param );
        } catch ( ParameterParsingException e ) {
            if ( match != null )
                fail( e.getMessage() );
        }
    }

}
