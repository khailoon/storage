/*
 * Copyright (c) 2005, David Peterson 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * 			 notice, this list of conditions and the following disclaimer in the
 *   		 documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 * 			 may be used to endorse or promote products derived from this software
 * 			 without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * v1.0: Created on Sep 21, 2005 by David Peterson
 */
package org.randombits.storage.servlet;

import java.util.Enumeration;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.randombits.storage.ObjectBasedStorage;

/**
 * This storage implementation uses the attribute feature of HttpServletRequest
 * objects to store and retrieve data. Any data stored will be available for the
 * duration of the current request.
 * 
 * @author David Peterson
 */
public class RequestAttributeStorage extends ObjectBasedStorage {
    private HttpServletRequest req;

    /**
     * Constructs a new Request Attribute storage object.
     * 
     * @param req
     *            The request object.
     */
    public RequestAttributeStorage( HttpServletRequest req ) {
        super( BoxType.Real );
        this.req = req;
    }

    @Override protected Set<String> baseNameSet() {
        Enumeration<String> e = req.getAttributeNames();
        Set<String> names = new java.util.HashSet<String>();
        while ( e.hasMoreElements() )
            names.add( e.nextElement() );
        return names;
    }

    @Override protected Object getBaseObject( String name ) {
        return req.getAttribute( name );
    }

    @Override public boolean isReadOnly() {
        return false;
    }

    @Override protected void setBaseObject( String name, Object value ) {
        req.setAttribute( name, value );
    }

}
