package org.randombits.storage.servlet;

import java.util.Enumeration;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.randombits.storage.StringBasedStorage;

public class RequestHeaderStorage extends StringBasedStorage {

    private HttpServletRequest req;

    private Set<String> headerNames;

    public RequestHeaderStorage( HttpServletRequest req ) {
        super( BoxType.Virtual );
        this.req = req;
    }

    @Override protected Set<String> baseNameSet() {
        if ( headerNames == null ) {
            headerNames = new java.util.HashSet<String>();
            Enumeration<String> names = req.getHeaderNames();
            while ( names.hasMoreElements() )
                headerNames.add( names.nextElement() );
        }
        return headerNames;
    }

    @Override protected String getBaseString( String name ) {
        return req.getHeader( name );
    }

    @Override protected void setBaseString( String name, String value ) {
        checkReadOnly();
    }

    @Override public boolean isReadOnly() {
        return true;
    }

}
